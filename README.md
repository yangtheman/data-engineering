# Data-Engineering Solution

_* This is assuming you have Ruby already installed. If not, follow this
[link](http://rvm.io/rvm/install) to install Ruby 1.9.3*_

## Installation

### Clone the repository

`git clone git@bitbucket.org:yangtheman/data-engineering.git`

### Create database and tables

`rake db:create`

`rake db:migrate`

### Install gems

`bundle install`

## Run the server

`bundle exec rails server`

## Access the server

Point the browser to `http://localhost:3000`
