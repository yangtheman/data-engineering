class CreateSalesData < ActiveRecord::Migration
  def change
    create_table :sales_data do |t|
      t.attachment :file
      t.decimal :total

      t.timestamps
    end
  end
end
