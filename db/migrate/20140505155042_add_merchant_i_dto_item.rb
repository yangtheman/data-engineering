class AddMerchantIDtoItem < ActiveRecord::Migration
  def change
    add_column :items, :merchant_id, :integer
    add_index :items, :merchant_id
  end
end
