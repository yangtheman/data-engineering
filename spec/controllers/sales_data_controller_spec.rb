require 'spec_helper'

describe SalesDataController do

  describe "GET :new" do
    before do
      get :new
    end

    it { should respond_with(:success) }
    it { should render_template(:new) }
  end

  describe "POST :create" do
    context "if file is attached" do
      before do
        file = fixture_file_upload('/example_input.tab')
        SalesData.any_instance.should_receive(:process_data).once
        post :create, :sales_data => {:file => file}
      end

      it { should redirect_to(sales_datum_path(assigns[:data])) }
    end

    context "if file is not attached" do
      before do
        post :create
      end

      it { should redirect_to(new_sales_datum_path) }
    end
  end

  describe "GET :show" do
    before do
      file = File.new("#{Rails.root}/spec/fixtures/example_input.tab")
      @data = SalesData.create(:file => file)
      get :show, :id => @data.id
    end

    it { should respond_with(:success) }
    it { should render_template(:show) }
  end

end
