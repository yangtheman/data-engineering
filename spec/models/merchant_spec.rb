require 'spec_helper'

describe Merchant do

  it { should have_many(:items) }
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }
  it { should validate_presence_of(:address) }

  describe '.get_or_create_by_name' do
    it 'should create a new merchant if not found' do
      merchant = Merchant.get_or_create_by_name("new merchant", "add1 add2")
      merchant.name.should == "new merchant"
      merchant.address.should == "add1 add2"
      Merchant.count.should == 1
    end

    it 'should return an existing purchaser if found' do
      Merchant.create(:name => 'old merchant', :address => 'add1 add2')
      merchant = Merchant.get_or_create_by_name("old merchant", 'add1 add2')
      merchant.name.should == "old merchant"
      merchant.address.should == 'add1 add2'
      Merchant.count.should == 1
    end
  end

  describe '#get_or_create_item' do
    before do
      @merchant = Merchant.create(:name => 'merchant', :address => 'add1')
    end

    it 'should create a new item if not found' do
      item = @merchant.get_or_create_item("new name", 1.0)
      item.description.should == "new name"
      item.price.should == 1.0
      Item.count.should == 1
    end

    it 'should return an existing purchaser if found' do
      @merchant.items.create(:description => 'old name', :price => 2.0)
      item = @merchant.get_or_create_item("old name", 2.0)
      item.description.should == "old name"
      item.price.should == 2.0
      Item.count.should == 1
    end
  end

end
