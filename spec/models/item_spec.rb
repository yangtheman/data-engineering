require 'spec_helper'

describe Item do

  it { should belong_to(:merchant) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:price) }
  it { should allow_value(1.0).for(:price) }
  it { should allow_value(0.0).for(:price) }
  it { should allow_value("1.0").for(:price) }
  it { should_not allow_value(-1.0).for(:price) }

end
