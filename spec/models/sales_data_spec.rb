require 'spec_helper'

describe SalesData do

  it { should have_attached_file(:file) }
  it { should validate_attachment_presence(:file) }

  before do
    file = File.new("#{Rails.root}/spec/fixtures/example_input.tab")
    @data = SalesData.create(:file => file)
  end

  describe '#read_file_content' do
    it 'should return an array of lines from the file' do
      result = @data.read_file_content
      result.is_a?(Array).should be_true
      result.size.should > 0
    end
  end

  describe '#process_data' do
    it 'should call process_line method n-1 times' do
      @data.should_receive(:process_line).exactly(4).times
      @data.process_data
    end

    it 'should create unique purchasers by name' do
      @data.process_data
      Purchaser.count.should == 3
    end

    it 'should create unique merchants by name' do
      @data.process_data
      Merchant.count.should == 3
    end

    it 'should create unique merchants by name' do
      @data.process_data
      Item.count.should == 3
    end

    it 'should create purchases' do
      @data.process_data
      Purchase.count.should == 4
    end

    it 'should sum up revenues' do
      @data.process_data
      @data.total.should == 95.0
    end
  end

end
