require 'spec_helper'

describe Purchase do

  it { should belong_to(:purchaser) }
  it { should belong_to(:item) }
  it { should validate_presence_of(:count) }
  it { should allow_value(1).for(:count) }
  it { should allow_value("1").for(:count) }
  it { should_not allow_value(0).for(:count) }

end
