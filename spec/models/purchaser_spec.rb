require 'spec_helper'

describe Purchaser do

  it { should have_many(:purchases) }
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }

  describe '.get_or_create_by_name' do
    it 'should create a new purchaser if not found' do
      purchaser = Purchaser.get_or_create_by_name("new name")
      purchaser.name.should == "new name"
      Purchaser.count.should == 1
    end

    it 'should return an existing purchaser if found' do
      Purchaser.create(:name => 'old name')
      purchaser = Purchaser.get_or_create_by_name("old name")
      purchaser.name.should == "old name"
      Purchaser.count.should == 1
    end
  end

end
