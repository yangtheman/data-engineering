class SalesDataController < ApplicationController

  def new
    @data = SalesData.new
  end

  def create
    if params[:sales_data]
      @data = SalesData.create(params[:sales_data])
      @data.process_data
      redirect_to sales_datum_path(@data)
    else
      redirect_to new_sales_datum_path
    end
  end

  def show
    @data = SalesData.find(params[:id])
  end

end
