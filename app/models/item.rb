class Item < ActiveRecord::Base

  before_save :convert_price_to_float

  belongs_to :merchant

  attr_accessible :description, :price

  validates :description, :presence => true
  validates :price,       :presence => true,
                          :numericality => { :greater_than_or_equal_to => 0 }

  private

  def convert_price_to_float
    price.to_f
  end

end
