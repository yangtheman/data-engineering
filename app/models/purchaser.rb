class Purchaser < ActiveRecord::Base

  has_many :purchases

  attr_accessible :name

  validates :name, :presence => true, :uniqueness => true

  def self.get_or_create_by_name(name)
    where(:name => name).first_or_create
  end

end
