class Purchase < ActiveRecord::Base

  before_save :convert_count_to_i

  belongs_to :purchaser
  belongs_to :item

  attr_accessible :count, :purchaser, :item

  validates :count, :presence => true,
                    :numericality => { :greater_than => 0 }

  private

  def convert_count_to_i
    count.to_i
  end

end
