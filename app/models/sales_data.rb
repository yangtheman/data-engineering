class SalesData < ActiveRecord::Base

  attr_accessible :file, :total

  has_attached_file :file, :processors => []

  validates_attachment :file, :presence => true

  def process_data
    lines = read_file_content
    total = normalize_data(lines)
    update_attributes(:total => total)
  end

  def read_file_content
    Paperclip.io_adapters.for(file).read.split("\n")
  end

  private

  def normalize_data(lines)
    sum = 0
    lines.each_with_index do |line, index|
      next unless index > 0
      line_sum = process_line(line)
      sum += line_sum if line_sum
    end
    sum
  end

  def line_split(line)
    line.strip.split("\t")
  end

  def process_line(line)
    array = line_split(line)
    purchaser = Purchaser.get_or_create_by_name(array[0])
    merchant = Merchant.get_or_create_by_name(array[5], array[4])
    item = merchant.get_or_create_item(array[1], array[2])
    purchase = Purchase.create(:purchaser => purchaser,
                               :item      => item,
                               :count     => array[3])
    item.price * purchase.count
  end

end
