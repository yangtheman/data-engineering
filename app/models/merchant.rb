class Merchant < ActiveRecord::Base

  has_many :items

  attr_accessible :address, :name

  validates :name,    :presence => true, :uniqueness => true
  validates :address, :presence => true

  def self.get_or_create_by_name(name, address)
    where(:name => name).first_or_create(:address => address)
  end

  def get_or_create_item(description, price)
    items.where(:description => description).first_or_create(:price => price)
  end

end
