DataEngineering::Application.routes.draw do

  resources :sales_data, :only => [:new, :create, :show]

  root :to => 'sales_data#new'

end
